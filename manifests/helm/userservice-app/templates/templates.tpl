# Name spacing templates is important because templates are _global_ and you can
# have collisions with other templates. This is particularly relevant if you
# pull in child charts with their own templates.
# {{- print "postgresql://" .address ":" .port "/" .name "?user=" .username "&password=" .password }}
# {{- print "postgresql://" .username ":" .password "@" .address ":" .port "/" .name }}
{{- define "userserviceApp.databaseConnectionString" }}
{{- with .Values.userserviceApp.database }}
{{- print "postgresql://" .address ":" .port "/" .name "?user=" .username "&password=" .password }}
{{- end }}
{{- end -}}
