# Name spacing templates is important because templates are _global_ and you can
# have collisions with other templates. This is particularly relevant if you
# pull in child charts with their own templates.
{{- define "postgresql.databaseConnectionString" }}
{{- with .Values }}
{{- print "postgres://" .auth.username ":" .auth.password "@" .address ":" .port "/" .auth.database }}
{{- end }}
{{- end -}}
