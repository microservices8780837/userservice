# userservice

# Sources

1. https://helm.sh/docs/topics/charts/
2. https://adamtheautomator.com/postgres-to-kubernetes/
3. https://www.fpcomplete.com/blog/yesod-postgres-kubernetes-deployment/
4. https://keithwade.com/posts/2022-12-06-practical_kubernetes_helm_and_helmfile/
5. https://www.youtube.com/watch?v=-lLT0vlaBpk&t=974s
6. https://github.com/bitnami/charts/blob/main/bitnami/postgresql/values.yaml - bitnami postgresql
7. https://hub.docker.com/r/bitnami/postgresql/tags - dockerhub bitnami/postgresql repo

---

# Домашнее задание

Инфраструктурные паттерны

# Цель:

В этом ДЗ вы создадите простейший RESTful CRUD.

# Описание/Пошаговая инструкция выполнения домашнего задания:

Сделать простейший RESTful CRUD по созданию, удалению, просмотру и обновлению пользователей.\
Пример API - https://app.swaggerhub.com/apis/otus55/users/1.0.0\
Добавить базу данных для приложения.\
Конфигурация приложения должна хранится в Configmaps.\
Доступы к БД должны храниться в Secrets.\
Первоначальные миграции должны быть оформлены в качестве Job-ы, если это требуется.\
Ingress-ы должны также вести на url arch.homework/ (как и в прошлом задании)\
На выходе должны быть предоставлена\

1. ссылка на директорию в github, где находится директория с манифестами кубернетеса
2. инструкция по запуску приложения.

* команда установки БД из helm, вместе с файлом values.yaml.
* команда применения первоначальных миграций
* команда kubectl apply -f, которая запускает в правильном порядке манифесты кубернетеса

3. Postman коллекция, в которой будут представлены примеры запросов к сервису на создание, получение, изменение и
   удаление пользователя. Важно: в postman коллекции использовать базовый url - arch.homework.
4. Проверить корректность работы приложения используя созданную коллекцию newman run коллекция_постман и приложить
   скриншот/вывод исполнения корректной работы

Задание со звездочкой:\
+5 балла за шаблонизацию приложения в helm чартах

---

# PostgreSQL

1. `helm search hub postgresql`
2. `helm repo add bitnami https://charts.bitnami.com/bitnami` - add Bitnami repository

```text
"bitnami" has been added to your repositories 
```

3. `helm repo update` - update Helm index charts
4. `helm repo list` - get list of help repositories

```text
NAME            URL
ingress-nginx   https://kubernetes.github.io/ingress-nginx/
bitnami         https://charts.bitnami.com/bitnami

```

There are 2 deploying types:

1) deploy a PostgreSQL database to Kubernetes using the `Helm chart`
2) manual deploy using `Deployment`

## Deploy a PostgreSQL database to Kubernetes using the Helm chart

1. Create PV and PVC - optional, default is created 8Gi

### PV and PVC

It is necessary to permanent data storage.

* PV – is a Kubernetes resource for storing data of your applications.
* PVC – is the way your application uses a given PV. PVC is used to mount a PV to your application’s pod.

- 1.1 `kubectl apply -f postgres-pv.yaml`
- 1.2 `kubectl apply -f postgres-pvc.yaml`
- 1.3 `kubectl get pv` - checking PersistentVolume
- 1.4 `kubectl get pvc` - checking PersistentVolumeClaim
- 1.5 `kubectl delete pv --all`

2. Create `values.yaml`

```text
#1. helm install postgresql bitnami/postgresql --set auth.username=sa,auth.password=sa,auth.database=userservice-app
#2. helm install userservice-app ./manifests/helm-variant/userservice-app/
userserviceApp:
  port: 8000

  database:
    username: sa
    password: sa
    address: postgresql.default.svc.cluster.local # host
    port: 5432
    name: userservice-app # db name
```

3. `helm install postgresql bitnami/postgresql --set auth.username=sa,auth.password=sa,auth.database=userservice-app` -
   deploy PostgreSQL to Kubernetes with the name3 postgresql-dev using the Bitnami Helm chart (bitnami/postgresql).\

   If error Permission denied, then:\
    * 3.1 `helm uninstall postgresql-dev`
    * 3.2 `helm install postgresql-dev -f values.yaml bitnami/postgresql --set volumePermissions.enabled=true`

      OR `helm install postgresql-test02 bitnami/postgresql --set persistence.existingClaim=postgresql-pv-claim --set volumePermissions.enabled=true`

4. `kubectl get pods` - checking pods
5. `kubectl get all`
6. `kubectl logs postgresql-dev-0` - checking logs of pods
7. `kubectl get secret --namespace default postgresql-dev -o jsonpath="{.data.password}" | base64 --decode` - check pswd
8. `export POSTGRES_PASSWORD=$(kubectl get secret --namespace default postgresql-dev -o jsonpath="{.data.password}" | base64 --decode)`
9. `kubectl run postgresql-dev-client --rm --tty -i --restart='Never' --namespace default --image docker.io/bitnami/postgresql:14.1.0-debian-10-r80 --env="PGPASSWORD=$POSTGRES_PASSWORD" \
   --command -- psql --host postgresql-dev -U sa -d otus -p 5432` - creates a new temporary pod called
   postgresql-dev-client that connects to the PostgreSQL database (otus) with the user sa.

* 9.1 `\conninfo` - checking PostgreSQL connection

```text
You are connected to database "otus" as user "sa" on host "postgresql-dev" (address "10.100.83.154") at port "5432"
```

* 9.2 `exit` - logout from PostgreSQL shell

## Manual

# ConfigMap

10. `echo 'postgres://demo:demo@postgresql.default.svc.cluster.local:5432/demo-express-app' | base64 -w 0` - get base64
11. Create ConfigMap

For the production environment, deploying secrets such as password authentication using the Kubernetes secret is highly
recommended for security.

* 11.1 `kubectl apply -f postgres-configmap.yaml`
* 11.2 `kubectl get cm`

### PV and PVC

12. Create PV needs to store data permanently.\

* PV – is a Kubernetes resource for storing data of your applications.
* PVC – is the way your application uses a given PV. PVC is used to mount a PV to your application’s pod.

- 12.1 `kubectl apply -f postgres-pv.yaml`
- 12.2 `kubectl apply -f postgres-pvc.yaml`
- 12.3 `kubectl get pv` - checking PersistentVolume
- 12.4 `kubectl get pvc` - checking PersistentVolumeClaim

### Deployment

13. Create Deployment postgres-deployment.yaml

* 13.1 `kubectl apply -f postgres-deployment.yaml`

14. `kubectl get deployments`
15. `kubectl get pods`

```text
$ kubectl get deployments
NAME       READY   UP-TO-DATE   AVAILABLE   AGE
postgres   1/1     1            1           4m42s
```

### Service

16. To let clients connect to the PostgreSQL pod, Service is needed

* 16.1 `kubectl apply -f postgres-service.yaml`
* 16.2 `kubectl get svc`

```text
kubectl get svc
NAME                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
postgres            NodePort    10.98.171.215   <none>        5432:32311/TCP   19s
```

### Start

```text
$ kubectl get pods
NAME                        READY   STATUS    RESTARTS   AGE
postgres-6785749444-pftpl   1/1     Running   0          19m
postgresql-dev-0            1/1     Running   0          44h
```

`kubectl exec -it postgres-xxxx -- psql -h localhost -U sa --password -p 5432 otus`

### Client

`psql -h 192.168.39.196 -U appuser --password -p 31398 appdb`

---

# Push userservice to dockerhub

1. `mvn clean install -DskipTests` OR `JAVA_HOME=C:/JAVA_UTILS/openjdk-17.0.1/ mvn clean install`
2. `export DOCKER_SCAN_SUGGEST=false` - turn off docker scan
3. `docker build -t userservice .` - create Docker image from Dockerfile and marks the image with a tag
4. `docker run -it -p 8080:8000 userservice` - check docker image
5. `docker tag userservice:latest coicoin/userservice:homework3` - To tag a local image "userservice" as "coicoin/userservice" with the tag "homework3":
6. `docker login`
7. `docker push coicoin/userservice:homework3` - push or share a local Docker image or a repository to a central repository

# Helm

1. `helm create userservice-app` - создать структуру helm
2. `tree userservice-app/`

```text
wordpress/
  Chart.yaml          # A YAML file containing information about the chart
  LICENSE             # OPTIONAL: A plain text file containing the license for the chart
  README.md           # OPTIONAL: A human-readable README file
  values.yaml         # The default configuration values for this chart
  values.schema.json  # OPTIONAL: A JSON Schema for imposing a structure on the values.yaml file
  charts/             # A directory containing any charts upon which this chart depends.
  crds/               # Custom Resource Definitions
  templates/          # A directory of templates that, when combined with values,
                      # will generate valid Kubernetes manifest files.
  templates/NOTES.txt # OPTIONAL: A plain text file containing short usage notes
```

2. `helm install userservice-app ./userservice-app --dry-run` - посмотреть что будет ставиться в кластер
3. `helm install userservice-app ./userservice-app` - установить приложение helm
4. `helm uninstall userservice-app` - delete
5. `helm history userservice-app` - просмотреть историю
6. `helm upgrade userservice-app ./userservice-app` - если поменять параметр `replicaCount: 2` в values.yaml хэлма,
   можно запустить обновление.
7. `helm pull chartrepo/chartname` - download and look at the files for a published chart, without installing it

---

# ConfigMap

For the production environment, deploying secrets such as password authentication using the Kubernetes secret is highly
recommended for security.

1. `kubectl apply -f postgres-configmap.yaml`
2. `kubectl get cm`
3. `winpty kubectl exec -it userservice-dp-6797c5584b-28gm8 -- sh` - start alpine
4. `kubectl exec -it userservice-dp-6797c5584b-28gm8 -- /bin/bash` - start sh
5. `env`
6. `cat ./config/dbconfig` - open dbconfig in volume

---

# Job

1. `kubectl apply -f userservice_job.yaml`
2. `kubectl logs kuber-job-jl24k`
3. `kubectl delete job  kuber-job`
4. `kubectl get pod --watch` - watching real time pods

---

# Change default helm message using templates/NOTE.txt

```text
Installed userservice-app chart.

App listening on port {{ .Values.userserviceApp.port }}.

App will look for database at "{{- print "postgres://" .Values.userserviceApp.database.address ":" .Values.userserviceApp.database.port "/" .Values.userserviceApp.database.name -}}"

One way of creating that PostgreSQL instance is to use the bitnami/postgresql chart.

    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm install postgresql bitnami/postgresql --set auth.username={{.Values.userserviceApp.database.username}},auth.password=<password>,auth.database={{.Values.userserviceApp.database.name}}
```

# helmfile

Source: https://keithwade.com/posts/2022-12-06-practical_kubernetes_helm_and_helmfile/

Sometimes installation order matters to get a smooth and fast startup without Kubernetes Pods crashing and being
recycled until their dependencies are available. These are all things that Helm file can help us with.

Helmfile is a declarative spec for deploying helm charts. It lets you keep a directory of chart value files, maintain
changes in version control, apply CI/CD to configuration changes, and sync to avoid skew in environments. I’ve found
Helmfile particularly useful for managing local development clusters.

```text
# ./9-helmfile/helmfile.yaml
repositories:
  # Need to define a source repository for our Bitnami chart. This lets us
  # declaratively define chart sources instead of having to manually configure
  # them via the helm command.
  - name: bitnami
    url: https://charts.bitnami.com/bitnami

releases:
  - name: demo-express-app
    # We'll reference the chart we created earlier
    chart: ../8-values
    # Provide values overrides
    values:
      - demoExpressApp:
          port: 4242
          database:
            username: cool-user
            password: hunter2
            name: its-a-cool-database
    # Tell Helmfile to finish setting up PostgreSQL before deploying our app's
    # chart
    needs:
      - postgresql

  - name: postgresql
    # Pull the `postgresql` chart from the `bitnami` repository we defined above
    chart: bitnami/postgresql
    # Provide values overrides
    values:
      - auth:
          username: cool-user
          password: hunter2
          database: its-a-cool-database
```

### Commands

1. `kubectl delete persistentvolumeclaims data-postgresql-0`
2. `helmfile sync --file k8s-demos/9-helmfile/helmfile.yaml`
3. `helmfile destroy --file k8s-demos/9-helmfile/helmfile.yaml`
4. `kc delete persistentvolumeclaims data-postgresql-0`

![img.png](img.png)

## Debug 

1. `kubectl exec -it userservice-app-v1-5846c659b7-hjj7x sh` 
2. `nslookup 10.106.199.51`
```text
Server:         10.96.0.10
Address:        10.96.0.10:53

51.199.106.10.in-addr.arpa      name = userservice-app-v1.default.svc.cluster.local
```

![img_1.png](img_1.png)

# Installing

1. `kubectl create namespace m && helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx/ && helm repo update && helm install nginx ingress-nginx/ingress-nginx --namespace m -f nginx-ingress.yaml`
2. `helm install userservice-app-postgresql bitnami/postgresql --set image.tag=14.10.0-debian-11-r17,auth.username=sa,auth.password=sa,auth.database=userservice-app && kubectl apply -f ./manifests/helm/userservice-app/postgres-service.yaml`
3. `helm install userservice-app ./manifests/helm/userservice-app/`
4. `helm uninstall userservice-app-postgresql`
5. `kubectl delete svc userservice-app-postgresql`
6. `helm uninstall userservice-app`
7. `kubectl delete pvc data-userservice-app-postgresql-0`

# Start

1. `http://arch.homework/swagger-ui` OR `http://arch.homework/swagger-ui/index.html` - swagger
2. `http://arch.homework/user/1` - GET by id
