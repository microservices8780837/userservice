package com.otus.userservice.controller;

import com.otus.userservice.dto.Message;
import com.otus.userservice.dto.UserCreationRequestDto;
import com.otus.userservice.dto.UserDto;
import com.otus.userservice.dto.UserUpdatingRequestDto;
import com.otus.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

  @Autowired
  private UserService userService;

  @PostMapping
  public ResponseEntity<UserDto> createUser(@RequestBody UserCreationRequestDto dto) {
    return new ResponseEntity<>(userService.saveUser(dto), HttpStatus.OK);
  }

  @GetMapping("/{userId}")
  public ResponseEntity<UserDto> getUser(@PathVariable("userId") Long userId) {
    return new ResponseEntity<>(userService.getUserById(userId), HttpStatus.OK);
  }

  @DeleteMapping("/{userId}")
  public ResponseEntity<Message> deleteUser(@PathVariable("userId") Long userId) {
    userService.deleteUser(userId);
    return new ResponseEntity<>(new Message(204, "User deleted"), HttpStatus.NO_CONTENT);
  }

  @PutMapping("/{userId}")
  public ResponseEntity<UserDto> updateUser(@PathVariable("userId") Long userId,
                                            @RequestBody UserUpdatingRequestDto dto) {
    return new ResponseEntity<>(userService.updateUser(userId, dto), HttpStatus.OK);
  }
}
