package com.otus.userservice.mapper;

import com.otus.userservice.dto.UserCreationRequestDto;
import com.otus.userservice.dto.UserDto;
import com.otus.userservice.dto.UserUpdatingRequestDto;
import com.otus.userservice.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
  User userCreationDtoToUser(UserCreationRequestDto dto);

  User userUpdatingDtoToUser(UserUpdatingRequestDto dto);

  UserCreationRequestDto userToUserCreationDto(User user);

  UserUpdatingRequestDto userToUserUpdatingDto(User user);

  UserDto userToUserDto(User user);
}
