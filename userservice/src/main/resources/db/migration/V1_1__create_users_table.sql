-- public.users definition

-- Drop table

-- DROP TABLE public.users;

CREATE TABLE authserviceapp.users
(
    id            bigserial    NOT NULL,
    username      varchar(128) NULL,
    first_name    varchar(128) NULL,
    last_name     varchar(128) NULL,
    email         varchar(128) NULL,
    phone         varchar(16)  NULL,
    creation_date timestamp    NULL,
    update_date   timestamp    NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id)
);